import Vue from "vue";
import App from "./app.vue";
import "../node_modules/normalize.css/normalize.css";
import "../node_modules/vue-multiselect/dist/vue-multiselect.min.css";
import "./styles.css";

export const getStats = async () => {
  const response = await fetch("./data.json");

  return response.json();
};

new Vue({
  el: "#app",
  render(createElement) {
    return createElement(App, {
      props: { statsPromise: getStats() },
    });
  },
});
