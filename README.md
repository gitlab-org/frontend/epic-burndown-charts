# Burndown charts for epics

This project builds burndown charts for epics.
While this tool could be used to visualize progress on all kind of epics and OKRs,
it is mainly tailored to work for our component migration OKRs.

OKRs are defined in [okrs.yml](./okrs.yml). Edit this file to ensure that e.g. team mappings are correct.

Currently resulting pages can be found here:

- **Current Quarter OKR**: https://gitlab-org.gitlab.io/frontend/epic-burndown-charts/fy23-q2/
- All time history: https://gitlab-org.gitlab.io/frontend/epic-burndown-charts/
- FY23-Q1 OKR: https://gitlab-org.gitlab.io/frontend/epic-burndown-charts/fy23-q1/
- FY22-Q4 OKR: https://gitlab-org.gitlab.io/frontend/epic-burndown-charts/fy22-q4/

## How this works

1. Querying all epics and sub epics and their issues (starting from the OKR definitions and [&3107])
2. Render SVG static charts for all OKRs + level 1 epics.
   These static charts can be used, e.g. in issue descriptions.
3. Putting the issue data into a JSON which is used for more interactive features like Leaderboards / Issues done per group.
4. Publish result to pages

## available executables

- `bin/build.js` Build the webapp (from `src/`) with esbuild. Also powers `yarn dev` for local development.
- `bin/get_data.js` Query data for the OKRs & [&3107]
- `bin/mark_issues_ready.js` Helper to mark all sub-issues of an epic to be ready for implementation

[&3107]: https://gitlab.com/groups/gitlab-org/-/epics/3107
