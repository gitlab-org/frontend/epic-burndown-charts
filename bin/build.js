#!/usr/bin/env node
const path = require("path");

const vuePlugin = require("esbuild-vue");

const ROOT_DIR = path.join(__dirname, "..");
const SRC_DIR = path.join(ROOT_DIR, "src");
const TARGET_DIR = path.join(ROOT_DIR, "public");

const IS_PRODUCTION = process.env.NODE_ENV === "production";
const SHOULD_SERVE = process.argv.includes("--serve");

const buildOptions = {
  entryPoints: [path.join(SRC_DIR, "app.js"), path.join(SRC_DIR, "sus.js")],
  sourcemap: "external",
  bundle: true,
  outdir: TARGET_DIR,
  mainFields: ["source", "module", "main"],
  legalComments: "inline",
  plugins: [
    vuePlugin({
      extractCss: true,
      production: IS_PRODUCTION,
    }),
  ],
  define: {
    "process.env.NODE_ENV": JSON.stringify(
      IS_PRODUCTION ? "production" : "development"
    ),
  },
  loader: {
    ".svg": "file",
  },
  minify: IS_PRODUCTION,
};

if (SHOULD_SERVE) {
  require("esbuild")
    .serve({ servedir: TARGET_DIR }, buildOptions)
    .then((res) => {
      const host = res.host === "0.0.0.0" ? "localhost" : res.host;
      console.log(`Started web server on http://${host}:${res.port}/`);
    });
} else {
  require("esbuild").build(buildOptions);
}
