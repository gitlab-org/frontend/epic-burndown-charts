#!/usr/bin/env node
const fs = require("fs");
const path = require("path");

const { getIssues, GitLabAPI } = require("../lib/api/epics");
const { renderHTML } = require("../lib/render_html");

const ROOT_DIR = path.join(__dirname, "..");
const targetDir = path.join(ROOT_DIR, "public", "sus");

async function main() {
  fs.mkdirSync(targetDir, { recursive: true });

  const { data: coordinationIssue } = await GitLabAPI.get(
    "/v4/projects/278964/issues/359914"
  );

  const { data: description } = await GitLabAPI.post("/v4/markdown", {
    text: coordinationIssue.description,
    gfm: true,
    project: "gitlab-org/gitlab",
  });

  const links = [...description.html.matchAll(/href="(.+?)"/g)].map(
    (match) => match[1]
  );

  const issues = [
    ...(await getIssues({
      group: "gitlab-org",
      labels: ["severity::1", "SUS::Impacting"],
    })),
    ...(await getIssues({
      group: "gitlab-org",
      labels: ["severity::2", "SUS::Impacting"],
    })),
  ].map((issue) => {
    const trackedInPlanningIssue = links.some((link) =>
      link.endsWith(issue.webPath)
    );

    return {
      ...issue,
      trackedInPlanningIssue,
    };
  });

  const data = {
    target: 57,
    coordinationIssue: "https://gitlab.com/gitlab-org/gitlab/-/issues/359914",
    issues,
  };

  fs.writeFileSync(
    path.join(targetDir, "data.json"),
    JSON.stringify(data),
    "utf-8"
  );

  const appPath = path.relative(
    path.join(targetDir),
    path.join(ROOT_DIR, "public", "sus.js")
  );

  fs.writeFileSync(
    path.join(targetDir, "index.html"),
    renderHTML({
      title: "SUS Issues Overview",
      appPath: appPath,
    }),
    "utf-8"
  );
}

main();
