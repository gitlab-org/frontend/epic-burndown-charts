#!/usr/bin/env node
const fs = require("fs");
const path = require("path");
const jsYAML = require("js-yaml");
const { renderHTML } = require("../lib/render_html");
const { renderChart } = require("../lib/render_static_chart");

const { DateTime } = require("../lib/date");

const { getEpic } = require("../lib/api/epics");
const {
  enhanceIssues,
  groupByAssignee,
  groupPeopleByTeam,
} = require("../lib/analysis");
const { ONGOING, CLOSED, ASSIGNED } = require("../lib/constants");

const ROOT_DIR = path.join(__dirname, "..");

const cache = {};

const getComponentFromLabels = (labels) =>
  labels.find((x) => x.startsWith("component:")) || null;

async function processRawEpic(group, epic, level = 0) {
  const { iid, webUrl, titleHtml } = epic;
  const cacheKey = `${group}|${iid}`;
  if (cache[cacheKey]) {
    return cache[cacheKey];
  }

  let { issues, labels, children } = epic;

  const epicComponent = getComponentFromLabels(labels);

  issues = issues.map(({ weight, createdAt, closedAt, ...rest }) => ({
    ...rest,
    epic: epic.iid,
    category:
      epicComponent || getComponentFromLabels(rest.labels || []) || "unknown",
    weight: weight || 1,
    createdAt: DateTime.fromISO(createdAt),
    closedAt: closedAt ? DateTime.fromISO(closedAt) : closedAt,
  }));

  if (level === 0) {
    issues = [];
  }

  children = (
    await Promise.all(children.map((iid) => getEpicData(group, iid, level + 1)))
  ).flat();

  issues = [...children.map((x) => x.issues).flat(), ...issues].reduce(
    (acc, issue) => {
      acc[issue.iid] = issue;
      return acc;
    },
    {}
  );

  cache[cacheKey] = [
    {
      iid,
      labels,
      issues: Object.values(issues),
      children,
      webUrl,
      titleHtml,
      level,
    },
    ...children,
  ];

  return cache[cacheKey];
}

async function getEpicData(group, epicIID, level) {
  const epic = await getEpic({ epicIID, group });

  return processRawEpic(group, epic, level);
}

function processEpic(epic, render) {
  const { startDate } = render;
  const { issues: issuesRaw, webUrl, iid, titleHtml, level } = epic;
  let { endDate = DateTime.now() } = render;
  if (endDate > DateTime.now()) {
    endDate = DateTime.now();
  }

  if (!issuesRaw.length) {
    console.warn(`No issues for ${webUrl}, aborting`);
    return [];
  }

  const transactions = issuesRaw
    .flatMap(({ closedAt, createdAt, category }) => {
      const ret = { date: createdAt, amount: 1, category };
      if (closedAt) {
        return [{ date: closedAt, amount: -1, category }, ret];
      }
      return ret;
    })
    .sort((b, a) => {
      if (a.date < b.date) {
        return 1;
      } else if (a.date > b.date) {
        return -1;
      }
      return 0;
    });

  const cats = [...new Set(transactions.map((x) => x.category))];

  const values = transactions.reduce((acc, curr) => {
    const { date, amount, category } = curr;
    const pointer = acc.pointer;
    const day = date.toISODate();

    if (pointer !== day) {
      for (const cat of cats) {
        const balance = acc[cat][pointer] || 0;
        acc.pointer = day;
        acc[cat][day] = balance;
      }
    }

    acc[category][day] += amount;

    return acc;
  }, Object.fromEntries(cats.map((x) => [x, {}])));

  let hasEntries = false;

  const firstDateToRender = startDate.minus({ months: 1 });
  let lastDateToRender = endDate.plus({ months: 1 });

  if (lastDateToRender > DateTime.now()) {
    lastDateToRender = DateTime.now();
  }

  const series = Object.entries(values).flatMap(([name, rawData]) => {
    if (name === "pointer") {
      return [];
    }

    const entries = Object.entries(rawData);

    const data = [];

    let i = entries.length - 1;

    if (
      entries.length &&
      (DateTime.fromISO(entries[i][0]) >= firstDateToRender ||
        entries[i][1] > 0)
    ) {
      hasEntries = true;

      do {
        const currDate = DateTime.fromISO(entries[i][0]);
        if (currDate > lastDateToRender) {
          i -= 1;
          continue;
        }
        if (data.length === 0) {
          data.push([lastDateToRender.toISODate(), entries[i][1]]);
        }
        if (currDate <= firstDateToRender) {
          data.unshift([firstDateToRender.toISODate(), entries[i][1]]);
          break;
        }
        data.unshift(entries[i]);
        i -= 1;
      } while (i >= 0);

      if (data.length && data[0][0] > startDate.toISODate()) {
        data.unshift([firstDateToRender.toISODate(), 0]);
      }
    }

    return {
      name,
      data,
    };
  });

  if (!hasEntries) {
    return [];
  }

  const issues = issuesRaw.filter(({ closedAt, createdAt }) => {
    // If the issue got created _after_ our interval ends,
    // don't consider it
    if (DateTime.fromISO(createdAt) > endDate) {
      return false;
    }

    // If the issue is open, we need to count it
    if (!closedAt) {
      return true;
    }

    // If the issue is open, was closed before our start Date, we do not need to consider it.
    return DateTime.fromISO(closedAt) >= startDate;
  });

  const closed = issues.filter(
    ({ closedAt }) => !!closedAt && DateTime.fromISO(closedAt) < endDate
  ).length;

  const percent = ((closed / issues.length) * 100).toFixed(1);

  let titleShort = `${closed} of ${issues.length} issues closed (${percent}%)`;

  if (closed === issues.length) {
    titleShort = `${issues.length} issued closed (100%)`;
  }

  const prefix = /^\d+$/.test(`${iid}`) ? `Epic &${iid}` : iid;

  const title = `${prefix}: ${titleShort}`;

  const svgFileName =
    webUrl
      .replace("https://gitlab.com/groups/", "")
      .replace("https://gitlab.com/", "")
      .replace("/-/issues/", "/")
      .replace("/-/epics/", "/")
      .replace(/[^A-Z0-9]/gi, "-")
      .replace(/-+/g, "-") + ".svg";

  return {
    webUrl,
    iid,
    series,
    title,
    svgFileName,
    titleHtml,
    titleShort,
    issues,
    level,
  };
}

function renderStatusSVG(epic) {
  return `<svg viewBox="0 0 240 20" xmlns="http://www.w3.org/2000/svg">
    <style>
    .small { font: italic 13px sans-serif; }
    </style>
  <text x="5" y="15" class="small">${epic.titleShort}</text>
  </svg>`;
}

async function main() {
  const OKRs = jsYAML
    .load(fs.readFileSync(path.join(ROOT_DIR, "okrs.yml")))
    .map((x) => {
      x.iid = x.id;
      x.prefix = x.id;
      x.startDate = DateTime.fromISO(x.startDate).startOf("day");
      x.endDate = DateTime.fromISO(x.endDate).endOf("day");
      x.status = DateTime.now() > x.endDate ? "FINISHED" : "ONGOING";
      x.markLine = true;
      return x;
    });
  let epicsList = [];

  for (const okr of OKRs) {
    epicsList = [
      ...epicsList,
      ...(await processRawEpic("gitlab-org", {
        ...okr,
        labels: [],
        issues: [],
      })),
    ];
  }

  epicsList = [...epicsList, ...(await getEpicData("gitlab-org", "3107"))];

  const epics = epicsList.reduce((acc, epic) => {
    acc.set(epic.iid + epic.group, epic);
    return acc;
  }, new Map());

  const renders = [
    {
      title: "All time progress",
      prefix: "",
      startDate: DateTime.fromISO("2020-10-01").startOf("day"),
    },
    ...OKRs,
  ];

  for (const render of renders) {
    const { children = [] } = render;

    const processed = [...epics.values()]
      .sort((a, b) => a.level - b.level)
      .filter(
        (a) =>
          a.level < 2 && (a.iid === render.prefix || /^\d+$/.test(`${a.iid}`))
      )
      .flatMap((e) => processEpic(e, render));

    const target = path.join(ROOT_DIR, "public", render.prefix);

    fs.mkdirSync(path.join(target, "/svgs/"), { recursive: true });
    fs.mkdirSync(path.join(target, "/json/"), { recursive: true });

    processed.forEach((definition) => {
      const { svgFileName, webUrl } = definition;
      console.log(`Writing SVG for ${webUrl}: ${svgFileName}`);
      fs.writeFileSync(
        path.join(target, "/svgs/", svgFileName),
        renderChart(definition, { ...render })
      );

      fs.writeFileSync(
        path.join(target, "/json/", svgFileName + ".json"),
        JSON.stringify(definition, null, 2)
      );

      fs.writeFileSync(
        path.join(target, "/svgs/", "status-" + svgFileName),
        renderStatusSVG(definition)
      );
    });

    const appPath = path.relative(
      path.join(target),
      path.join(ROOT_DIR, "public", "app.js")
    );

    fs.writeFileSync(
      path.join(target, "/index.html"),
      renderHTML({ ...render, appPath })
    );

    const issues = enhanceIssues(
      processed[0]?.issues || [],
      render.status === ONGOING
    );
    const perGroup = groupPeopleByTeam(
      groupByAssignee(issues),
      render.groups || {}
    );

    fs.writeFileSync(
      path.join(target, "/data.json"),
      JSON.stringify({
        ...render,
        issues,
        epics: processed.map(
          ({ webUrl, svgFileName, titleHtml, title, iid, level, ...rest }) => ({
            primary:
              level === 0 || children.length === 0 || children.includes(iid),
            webUrl,
            svgFileName,
            titleHtml,
            title,
          })
        ),
      })
    );

    fs.writeFileSync(
      path.join(target, "stats.csv"),
      [
        ["Name", "Closed", "Assigned", "Target"],
        ...Object.entries(perGroup).map(([, val]) => [
          val.name,
          val[CLOSED],
          val[ASSIGNED],
          val.target,
        ]),
      ]
        .map((x) => `"${x.join('";"')}"`)
        .join("\n")
    );
  }
}

main()
  .then(() => {
    console.log("Success");
  })
  .catch((e) => {
    console.error("An error happened");
    console.error(e);
    process.exit(1);
  });
