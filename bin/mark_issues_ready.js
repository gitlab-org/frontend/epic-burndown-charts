#!/usr/bin/env node
const { program, InvalidArgumentError } = require("commander");
const { getEpic } = require("../lib/api/epics");
const asyncPool = require("tiny-async-pool");
const { GitLabAPI } = require("gitlab-api-async-iterator")();

const labelsToAdd = [
  "pajamas::integrate",
  "OKR",
  "group::foundations",
  "type::maintenance",
  "severity::4",
];

async function ensureLabels(issue) {
  const [projectPath] = issue.webPath.substr(1).split("/-/issues/");

  const issueAPIURL = `/projects/${encodeURIComponent(projectPath)}/issues/${
    issue.iid
  }`;

  if (issue.closedAt) {
    console.log(`${issue.webPath}: Issue closed, no need to update labels`);
    return;
  }

  const { data } = await GitLabAPI.get(issueAPIURL);
  const missingLabels = subtract(labelsToAdd, data.labels);

  if (missingLabels.length > 0) {
    console.log(
      `${issue.webPath}: Adding missing labels to issue: ${missingLabels.join(
        ", "
      )}`
    );
    return GitLabAPI.put(issueAPIURL, {
      add_labels: missingLabels.join(","),
    });
  }

  console.log(`${issue.webPath}: All Labels are set`);
}

function subtract(list, other) {
  const result = new Set(list);

  for (const item of other) {
    result.delete(item);
  }

  return Array.from(result);
}

async function main(epicIID) {
  const { issues } = await getEpic({ epicIID, group: "gitlab-org" });
  await asyncPool(10, issues, ensureLabels);
}

function ensureInt(value) {
  // parseInt takes a string and a radix
  const parsedValue = parseInt(value, 10);
  if (isNaN(parsedValue)) {
    throw new InvalidArgumentError("Not a number.");
  }
  return parsedValue;
}

program
  .description(
    'A simple script in order to mark issues of component migrations as "ready-for-development"'
  )
  .argument("<epic iid>", "Epic IID", ensureInt)
  .action(async (epicIID) => {
    try {
      await main(epicIID);
    } catch (e) {
      console.error("An error happened");
      console.log(e);
      process.exit(1);
    }
  })
  .parse();
