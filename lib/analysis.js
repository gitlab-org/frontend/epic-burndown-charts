const {
  UNASSIGNED,
  defaultStats,
  CLOSED,
  ONGOING,
  OPEN,
  READY_TO_BE_IMPLEMENTED,
  ASSIGNED,
  IGNORED,
  ISSUE_STATES_SUS,
} = require("./constants");

function groupByAssignee(issues) {
  return Object.values(
    issues.reduce((acc, current) => {
      const assignee = current?.assignees?.[0] || UNASSIGNED;
      acc[assignee] ||= { name: assignee, ...defaultStats };

      acc[assignee][current.issueStatus] += 1;
      acc[assignee].total += 1;

      return acc;
    }, {})
  );
}

function groupPeopleByTeam(folks, groupDefinitions) {
  const teamMap = getTeamMap(groupDefinitions);

  const map = {};

  for (const [name, group] of Object.entries(groupDefinitions)) {
    map[name] ||= {
      ...defaultStats,
      name,
      link: group.okr || null,
      folks: [],
      target: group?.target || 0,
      groups: (group?.groups || []).sort(),
      completion: -1,
    };
  }

  for (const curr of folks) {
    const name = teamMap[curr.name] || "unknown";

    map[name] ||= {
      ...defaultStats,
      name,
      folks: [],
      target: 0,
      groups: [],
      completion: -1,
    };

    for (const key of Object.keys(defaultStats)) {
      map[name][key] += curr[key];
    }
    if (map[name].target) {
      map[name].completion = map[name].closed / map[name].target;
    }
    map[name].folks.push(curr.name);
    map[name].folks.sort();
  }

  return Object.values(map);
}

function getTeamMap(groups) {
  return Object.fromEntries(
    Object.entries(groups).flatMap(
      ([name, { managers = [], members = [], others = [] }]) => {
        return [
          ...managers.map((x) => [x, name]),
          ...members.map((x) => [x, name]),
          ...others.map((x) => [x, name]),
        ];
      }
    )
  );
}

function enhanceIssues(issues, onGoing = false) {
  return issues.map((current) => {
    const assignee = current?.assignees?.[0] || UNASSIGNED;
    const labels = current?.labels || [];

    if (labels.includes("meta") || labels.includes("wontfix")) {
      current.issueStatus = IGNORED;
    } else if (!!current.closedAt) {
      current.issueStatus = CLOSED;
    } else if (!onGoing) {
      current.issueStatus = OPEN;
    } else if (assignee === UNASSIGNED) {
      if (labels.includes("OKR")) {
        current.issueStatus = READY_TO_BE_IMPLEMENTED;
      } else {
        current.issueStatus = OPEN;
      }
    } else {
      current.issueStatus = ASSIGNED;
    }

    return current;
  });
}

function enhanceSUSIssues(issues) {
  return issues.map((current) => {
    const labels = current?.labels || [];

    const workflow = labels.find((x) => x.startsWith("workflow::"));

    if (!!current.closedAt) {
      current.issueStatus = ISSUE_STATES_SUS.CLOSED;
    } else if (workflow) {
      switch (workflow) {
        case "workflow::start":
        case "workflow::problem validation":
        case "workflow::validation backlog":
        case "workflow::design":
        case "workflow::solution validation":
        case "workflow::planning breakdown":
        case "workflow::ready for design":
        case "workflow::refinement":
        case "workflow::scheduling":
          current.issueStatus = ISSUE_STATES_SUS.IN_VALIDATION;
          break;
        case "workflow::ready for development":
        case "workflow::in dev":
        case "workflow::in review":
        case "workflow::verification":
          current.issueStatus = ISSUE_STATES_SUS.IN_DEVELOPMENT;
          break;
        case "workflow::blocked":
          current.issueStatus = ISSUE_STATES_SUS.BLOCKED;
          break;
        default:
          current.issueStatus = ISSUE_STATES_SUS.UNKNOWN;
      }
    } else {
      current.issueStatus = ISSUE_STATES_SUS.OPEN;
    }

    current.groups = labels.filter((x) => x.startsWith("group:"));

    return current;
  });
}

module.exports = {
  groupPeopleByTeam,
  groupByAssignee,
  enhanceIssues,
  enhanceSUSIssues,
};
