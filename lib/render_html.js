const renderHTML = ({ title, startDate, endDate, appPath }) => {
  const t = [
    title,
    startDate
      ? `(${startDate.toISODate()} until ${
          endDate ? endDate.toISODate() : "open end"
        })`
      : null,
  ]
    .filter(Boolean)
    .join(" ");

  const cssPath = appPath.replace(".js", ".css");

  return `<html lang="en">
<head>
  <meta charset="utf-8">
  <title>${t}</title>
  <script src="${appPath}" defer></script>
  <link rel="stylesheet" href="${cssPath}" type="text/css"></link>
</head>
<body>
  <h1>${t}</h1>
  <div id="app"></div>
</body>
</html>`;
};

module.exports = {
  renderHTML,
};
