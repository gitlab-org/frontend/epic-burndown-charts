const { DateTime, Settings } = require("luxon");

Settings.defaultZone = "UTC";

module.exports = {
  DateTime,
  Settings,
};
