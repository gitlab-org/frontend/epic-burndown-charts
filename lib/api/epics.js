const factory = require("gitlab-api-async-iterator");
const { GitLabAPI } = factory({ baseURL: "https://gitlab.com/api/" });

const issuePartial = (type = "Issue") => `
fragment IssuePartial on ${type} {
  iid
  assignees {
    nodes {
      username
    }
  }
  labels {
    nodes {
      title
    }
  }
  milestone {
    startDate
    dueDate
    title
  }
  weight
  createdAt
  closedAt
  webPath
}
`;

const issueQuery = `
${issuePartial()}

query ($group: ID!, $labels: [String], $after: String) {
  group(fullPath: $group) {
    issues(labelName: $labels, includeSubgroups: true, after: $after) {
      pageInfo {
        endCursor
        hasNextPage
      }
      nodes {
        ...IssuePartial
        titleHtml
        confidential
      }
    }
  }
}
`;

const epicIssuesQuery = `
${issuePartial("EpicIssue")}
query ($iid: ID, $group: ID!, $after: String) {
  group(fullPath: $group) {
    epics(iid: $iid, includeDescendantGroups: true) {
      nodes {
        issues(after: $after) {
          count
          pageInfo {
            endCursor
            hasNextPage
          }
          nodes {
            ...IssuePartial
          }
        }
      }
    }
  }
}
`;

const epicQuery = `

query ($iid: ID, $group: ID!) {
  group(fullPath: $group) {
    epics(iid: $iid, includeDescendantGroups: true) {
      nodes {
        iid
        titleHtml
        webUrl
        labels {
          nodes {
            title
          }
        }
        children(confidential: false) {
          nodes {
            iid
          }
        }
      }
    }
  }
}
`;

async function graphql({ query, variables = {} }) {
  const { data } = await GitLabAPI.post("/graphql", {
    query,
    variables,
  });

  return data;
}

function mapLabels(labels) {
  return labels?.nodes?.map((x) => x.title) || [];
}

function mapIssues(issues) {
  return (
    issues?.nodes?.map(({ assignees, labels, ...rest }) => {
      const safeTitle = {};

      if (rest.titleHtml) {
        safeTitle.titleHtml = rest.confidential
          ? "[Confidential]"
          : rest.titleHtml;
      }

      return {
        labels: mapLabels(labels),
        assignees: assignees?.nodes?.map(({ username }) => username) || [],
        ...rest,
        ...safeTitle,
      };
    }) || []
  );
}

async function getEpic({ group, epicIID }) {
  const { data } = await graphql({
    query: epicQuery,
    variables: {
      iid: epicIID,
      group,
    },
  });

  const epic = data.group.epics.nodes[0];
  let { labels, children, iid } = epic;
  labels = mapLabels(labels);
  children = children.nodes.map(({ iid }) => iid);

  let issues = await getEpicIssues({ group: group, epicId: iid });

  return { ...epic, children, labels, issues };
}

async function getEpicIssues({ group, after = "", epicId }) {
  const { data } = await graphql({
    query: epicIssuesQuery,
    variables: {
      iid: epicId,
      group,
      after,
    },
  });

  let { epics } = data.group;
  let { issues } = epics.nodes[0];
  const { pageInfo } = issues;

  issues = mapIssues(issues);

  if (pageInfo.hasNextPage) {
    return [
      ...issues,
      ...(await getEpicIssues({ group, after: pageInfo.endCursor, epicId })),
    ];
  }

  return issues;
}

async function getIssues({ group, labels, after = "" }) {
  console.log(`Cursor: ${after}`);
  const { data } = await graphql({
    query: issueQuery,
    variables: {
      labels,
      group,
      after,
    },
  });

  let { issues } = data.group;
  const { pageInfo } = issues;

  issues = mapIssues(issues);

  if (pageInfo.hasNextPage) {
    return [
      ...issues,
      ...(await getIssues({ group, labels, after: pageInfo.endCursor })),
    ];
  }

  return issues;
}

module.exports = {
  getEpic,
  GitLabAPI,
  getIssues,
};
