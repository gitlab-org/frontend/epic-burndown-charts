import { colorFromDefaultPalette } from "@gitlab/ui/src/utils/charts/theme.js";

export { colorFromDefaultPalette };
