let helpers;
const echarts = require("echarts");
const { escape } = require("lodash");
const { DateTime } = require("./date");

try {
  helpers = require("./cjs/helpers");
} catch (e) {
  console.error("Could not load ./cjs/helpers, please run `yarn run build`");
  process.exit(1);
}

const labelFormatter = function (value) {
  if (value && value.value) {
    value = value.value;
  }
  return DateTime.fromMillis(value).toISODate();
};

const { colorFromDefaultPalette } = helpers;

const renderChart = (data, options = {}) => {
  const { width = 1600, height = 900, markLine = false } = options;

  let markLineOptions = false;

  if (markLine) {
    const { startDate = false, endDate = false } = options;
    markLineOptions = {
      data: [startDate, endDate].flatMap((date) => {
        if (date && date < DateTime.now()) {
          return {
            label: {
              formatter: date.toISODate(),
              position: "end",
            },
            xAxis: date.toMillis(),
          };
        }
        return [];
      }),
    };
  }

  const chart = echarts.init(null, null, {
    renderer: "svg",
    ssr: true,
    width: width,
    height: height,
  });

  chart.setOption({
    title: {
      text: escape(data.title),
      top: "top",
      left: "center",
    },
    tooltip: {},
    legend: {
      data: data.series.map(({ name }) => {
        return { name, lineStyle: { width: 5 } };
      }),
      top: "center",
      left: "left",
      orient: "vertical",
      itemGap: 30,
      padding: 5,
    },
    grid: {
      left: 275,
    },
    xAxis: {
      type: "time",
      axisLabel: {
        formatter: labelFormatter,
      },
    },
    yAxis: {},
    series: data.series.map(({ name, data }, index) => {
      const color = colorFromDefaultPalette(index);
      return {
        name,
        data,
        areaStyle: {
          opacity: 0.2,
          color,
        },
        showSymbol: false,
        lineStyle: {
          color,
        },
        itemStyle: {
          color,
        },
        markLine: markLineOptions,
        animation: false,
        type: "line",
        stack: "stack",
      };
    }),
  });

  const svg = chart.renderToSVGString();

  chart.dispose();

  return svg;
};

module.exports = {
  renderChart,
};
